import QtQuick 2.0
import QtGraphicalEffects 1.0
Item {
    property string color: ""
    Rectangle{
        id: ball
        anchors.fill: parent
        radius: Math.max(width,height) / 2
        color: parent.color
    }


    RadialGradient {
            source: ball
           anchors.fill: parent

           gradient: Gradient {
               GradientStop { position: 0.0; color: "white" }
               GradientStop { position: 0.8; color: ball.color }
           }
      }
}
