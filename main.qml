import QtQuick 2.6
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

ApplicationWindow {

    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    SwipeView {
        id: swipeView
        anchors.fill: parent


        Page {
            Field{
                id: field
                width: Math.min(parent.height,parent.width)
                height: Math.min(parent.height,parent.width)
            }
        }

        Page {
            Settings{
                anchors.fill: parent
                onApplySignal: {
                    field.maxHCell = row
                    field.maxWCell = col
                    field.randBall = randBall
                    field.maxBalls = maxBalls
                    field.startNewGame()
                }
            }
        }
    }


}
