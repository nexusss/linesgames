import QtQuick 2.0
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import "JS/Field.js" as FieldJS

Item {
    property int maxWCell: 9
    property int maxHCell: 9
    property int randBall: 3
    property int maxBalls: 4
    property int freeCells: maxWCell * maxHCell
    property int score: 0

    property string moveColor: ""
    property variant colorList: ["Yellow","Red","Blue","lightBlue","Green","Orange","Mediumpurple"]
    property variant field: [[]]

    signal gameOver()

    ListModel {
        id: cellsModel;
    }
    Component {
         id: highlight

         Rectangle {
             width: gridView1.cellWidth
             height: gridView1.cellHeight
             color: "lightsteelblue"
             radius: 5
             border.width: 2
             x: gridView1.currentItem.x
             y: gridView1.currentItem.y
             z: 1
             Ball{
                 id: highlightBall
                 anchors.right: parent.right
                 anchors.rightMargin: parent.width * 0.2
                 anchors.left: parent.left
                 anchors.leftMargin: parent.width * 0.2
                 anchors.bottom: parent.bottom
                 anchors.bottomMargin: parent.height * 0.2
                 anchors.top: parent.top
                 anchors.topMargin: parent.height * 0.2
                 color: moveColor

                 visible: color !== ""

             }
             Behavior on x { SpringAnimation {
                     id: xAnim
                     spring: 3;
                     damping: 0.2;
                     onStopped: console.log("stopx")
                     onStarted: console.log("startx")
                     onRunningChanged:
                         if(!yAnim.running && !running){
                             console.log("runx",gridView1.currentIndex,cellsModel.get(gridView1.currentIndex).newBallColor,highlightBall.color)
                            cellsModel.get(gridView1.currentIndex).newBallColor = highlightBall.color
                             moveFin(gridView1.currentIndex)
                            gridView1.currentIndex = -1
                            moveColor = ""
                            gridView1.enabled = true}} }
             Behavior on y { SpringAnimation {
                     id: yAnim
                     spring: 3; damping: 0.2;
                     onStopped: console.log("stopy")
                     onStarted: console.log("starty")
                     onRunningChanged:
                         if(!yAnim.running && !running){
                             console.log("runy")
                            cellsModel.get(gridView1.currentIndex).newBallColor = highlightBall.color
                            moveFin(gridView1.currentIndex)
                            gridView1.currentIndex = -1
                            moveColor = ""
                            gridView1.enabled = true
                            }} }
         }

     }
    GridView {
        id: gridView1
        anchors.fill: parent
        highlight: highlight
              highlightFollowsCurrentItem: false
              focus: true
        cellHeight: Math.max(parent.height / maxHCell,parent.width / maxWCell)
        cellWidth: Math.max(parent.height / maxHCell,parent.width / maxWCell)
        delegate: Rectangle {
                    property string ballColor: newBallColor
                    width: gridView1.cellWidth
                    height: gridView1.cellHeight
                    color: "gray"

                    radius: 5
                    border.width: 2
                    Ball{
                        anchors.right: parent.right
                        anchors.rightMargin: parent.width * 0.2
                        anchors.left: parent.left
                        anchors.leftMargin: parent.width * 0.2
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: parent.height * 0.2
                        anchors.top: parent.top
                        anchors.topMargin: parent.height * 0.2
                        color: parent.ballColor
                        visible: color !== ""

                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {

                            if(gridView1.currentIndex === index || (parent.ballColor === "" && gridView1.currentIndex === -1)){//если нажат выбранный шар, или попытка выбора там где нет шара
                                gridView1.currentIndex = -1
                            }
                            else{

                                if(cellsModel.get(index).newBallColor === "" || gridView1.currentIndex === -1){//если это не попытка переместиться на позицую с шаром
                                    if(gridView1.currentIndex !== -1){//если это перемещние шара
                                        cellsModel.get(gridView1.currentIndex).newBallColor = ""
                                        gridView1.enabled = false
                                    }
                                    else//если это выбор шара
                                        moveColor = cellsModel.get(index).newBallColor

                                    gridView1.currentIndex = index
                                }
                            }
                        }
                    }
        }

        model: cellsModel

    }

    Component.onCompleted: {
        startNewGame()
    }

    function startNewGame(){
        initArray()
        createField()
        score = 0
        freeCells = maxWCell * maxHCell
    }

    function initArray(){

        field = new Array(maxHCell)
        for(var i = 0; i < maxHCell; i++){
            field[i] = new Array(maxWCell)
        }
    }

    function createField(){
        cellsModel.clear()
        for(var i = 0 ; i < maxWCell; i++)
            for(var j = 0; j < maxHCell; j++){
                cellsModel.append({newBallColor: ""})
                field[i][j] = cellsModel.get(cellsModel.count - 1)
            }

        addRandBall()
        gridView1.currentIndex = -1

    }

    function getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
    }

    function addRandBall(){
        console.log("freeCellsa",freeCells)
        if(freeCells <= randBall)
            return;

        for(var i = 0; i < randBall; i++){
            var color = colorList[getRandomInt(0,colorList.length)]
            console.log("color",color)

            var pos;
            do{
                pos = getRandomInt(0,maxWCell * maxHCell)
            }while(cellsModel.get(pos).newBallColor !== "")

            cellsModel.get(pos).newBallColor = color.toString()
            freeCells--
            checkCells(pos)
        }
        console.log("freeCellsb",freeCells)
//        console.log("field")
//        for(var i = 0 ; i < maxWCell; i++){
//            console.log("i", i)
//            for(var j = 0; j < maxHCell; j++){
//                console.log("j", j)
//                console.log(field[i][j].newBallColor)

//            }
//        }

    }

    function moveFin(listModelIndex){
        checkCells(listModelIndex)
        addRandBall()

    }

    function checkCells(listModelIndex){
        var rowDist = checkRow(listModelIndex)
        var colDist = checkCol(listModelIndex)
        console.log("rowDist",rowDist.left,rowDist.right,rowDist.row)
        console.log("colDist",colDist.up,colDist.down,colDist.col)
        var  countrowBalls = (rowDist.right - rowDist.left) + 1 //число шаров по горизонту
        var  countcolBalls = (colDist.down - colDist.up) + 1 //число шаров по вертикали
        if(countrowBalls >= maxBalls){
            freeCells += countrowBalls
            score += (countrowBalls * maxBalls)
            for(var i = rowDist.left; i <= rowDist.right; i++){
                field[rowDist.row][i].newBallColor = ""
            }
        }

        if(countcolBalls >= maxBalls){
            freeCells += countcolBalls
            score += (countcolBalls * maxBalls)
            for(var i = colDist.up; i <= colDist.down; i++){
                field[i][colDist.col].newBallColor = ""
            }
        }

    }

    function checkCol(listModelIndex){
        var arrIJ = FieldJS.getArrIJFromModelIndex(listModelIndex);
        var upPos = arrIJ[0];
        var downPos = arrIJ[0]
        var upColor = ""
        var downColor = ""
        var currentColor = ""
            for(var i = arrIJ[0]; i > 0 ; i--){
                upColor = field[i - 1][arrIJ[1]].newBallColor
                currentColor = field[i][arrIJ[1]].newBallColor
                if(upColor === currentColor){
                    upPos = i - 1;
                }
                else{
                    i = 0
                }
            }

            for(var i = arrIJ[0]; i < field.length - 1 ; i++){
                downColor = field[i + 1][arrIJ[1]].newBallColor
                currentColor = field[i][arrIJ[1]].newBallColor
                if(downColor === currentColor){
                    downPos = i + 1;
                }
                else{
                    i = field.length
                }
            }

        return {up: upPos,down: downPos,col: arrIJ[1]}
    }

    function checkRow(listModelIndex){
        var arrIJ = FieldJS.getArrIJFromModelIndex(listModelIndex);
        var leftPos = arrIJ[1];
        var rightPos = arrIJ[1]
        var leftColor = ""
        var rightColor = ""
        var currentColor = ""
            for(var i = arrIJ[1]; i > 0 ; i--){

                leftColor = field[arrIJ[0]][i - 1].newBallColor
                currentColor = field[arrIJ[0]][i].newBallColor
                if(leftColor === currentColor){
                    leftPos = i - 1;
                }
                else{
                    i = 0
                }
            }

            for(var i = arrIJ[1]; i < field[arrIJ[0]].length - 1 ; i++){
                rightColor = field[arrIJ[0]][i + 1].newBallColor
                currentColor = field[arrIJ[0]][i].newBallColor
                if(rightColor === currentColor){
                    rightPos = i + 1;
                }
                else{
                    i = field[arrIJ[0]].length
                }
            }

        return {left: leftPos,right: rightPos,row:arrIJ[0]}
    }
}
