import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1
Item {
    property int row: sliderRow.value
    property int col: sliderCol.value
    property int randBall: sliderBalls.value
    property int maxBalls: sliderMaxBalls.value
    signal applySignal()
    anchors.fill: parent
    ColumnLayout {
        anchors.fill: parent

        GridLayout {
            id: gridLayout1
            Layout.fillWidth: true
            Layout.fillHeight: true
            anchors.fill: parent
            columns: 3
            Text {

                text: qsTr("h cells")
                font.pixelSize: 28
                Layout.column: 0
                Layout.row: 0
            }


            MySlider {
                id: sliderRow

                spacing: 0
                topPadding: 6
                stepSize: 1
                to: 15
                from: 6
                Layout.fillWidth: true
                value: 9
                Layout.column: 2
                Layout.row: 0
                onValueChanged: {
                    applyButton.enabled = true
                }
            }

            Text {

                text: qsTr("w cells")
                font.pixelSize: 28
                Layout.column: 0
                Layout.row: 1
            }



            MySlider {
                id: sliderCol
                spacing: 0
                topPadding: 6
                stepSize: 1
                to: 15
                from: 6
                Layout.fillWidth: true
                value: 9
                Layout.column: 2
                Layout.row: 1
                onValueChanged: {
                    applyButton.enabled = true
                }
            }

            Text {

                text: qsTr("balls")
                font.pixelSize: 28
                Layout.column: 0
                Layout.row: 2
            }



            MySlider {
                id: sliderBalls
                spacing: 0
                topPadding: 6
                stepSize: 1
                to: Math.floor(Math.min(sliderRow.value,sliderCol.value) / 2)
                from: 1
                Layout.fillWidth: true
                value: 3
                Layout.column: 2
                Layout.row: 2
                onValueChanged: {
                    applyButton.enabled = true
                }
            }

            Text {

                text: qsTr("max bals")
                font.pixelSize: 28
                Layout.column: 0
                Layout.row: 3
            }

            MySlider {
                id: sliderMaxBalls
                spacing: 0
                topPadding: 6
                stepSize: 1
                to: Math.floor(Math.min(sliderRow.value,sliderCol.value))
                from: 2
                Layout.fillWidth: true
                value: 5
                Layout.column: 2
                Layout.row: 3
                onValueChanged: {
                    applyButton.enabled = true
                }
            }
        }


        Button {
            id: applyButton
            text: qsTr("Apply")
            Layout.fillWidth: false
            Layout.fillHeight: false
            anchors.horizontalCenter: parent.horizontalCenter
            enabled: false
            onClicked: {
                applyButton.enabled = false
                applySignal()
            }
        }
    }
}
